<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <h1>${uName}</h1>
    <form method="post" action="mainemp">        
        <h2> Empires: </h2>  <input type="submit" value="New empire" >
        <br><br> 
    </form>
    <c:forEach var="e" items="${empires}">
         <!--   <form method="post" action="editemp"> -->
            Empire name:${e.name} Empire description: ${e.description}
            <form method="post" action="mainbuildingtoemp">
            <input type="hidden"  value="${e.name}" name="eName"/>
            <input type="submit" value="Add building" >
            <br><br>
            </form> 
    </c:forEach>
    <br><br>
    <h2>Buildings: </h2> 
    <form method="post" action="mainbuilding">        
         <input type="submit" value="New building">
    </form>
    <br><br>
    <form method="post" action="listbuilding">        
         <input type="submit" value="List buildings" >
    </form>
    <br><br>
    <form method="post" action="searchbuilding">
         <input type="submit" value="Search">
         <input type="text" name="ID" > (ID)
         <br>
    </form>
    <br><br>
    <h2>Natural assets: </h2>
    <form method="post" action="mainasset">        
        <input type="submit" value="New asset" >
    </form>
     <br><br>
     <form method="post" action="listassets">        
         <input type="submit" value="List assets" >
    </form>
     <br><br>
    <form method="post" action="searchasset">
         <input type="submit" value="Search">
         <input type="text" name="ID" > (ID)
         <br>
    </form>
    <br><br>
    <form method="post" action="search">
        <h2> Search empires... </h2>
         <input type="checkbox" name="condition" value="IsName"> Name: <input type="text" name="Name" >
         <br><br>
         <input type="checkbox" name="condition" value="IsDesc"> Description: <input type="text" name="Desc" >
         <br><br>
         <input type="checkbox" name="condition" value="IsBuilding"> Building ID: <input type="text" name="Building" >
         <br><br>
         <input type="checkbox" name="condition" value="IsAsset">
         Natural asset:
         <select name="SelectedAsset">
           <c:forEach var="asset" items="${assets}">
            <option value="${asset.ID}">${asset.name}</option>
           </c:forEach>            
         </select>
         Min stock: <input type="text" name="Asset" >
         <br><br>
         <input type="submit" value="Search">
         <br><br>
    </form>
    <br><br>
    <form method="post" action="mainhero">        
        Heroes: <input type="submit" value="Uj hos" >
        <br>       
    </form>
        <c:forEach var="h" items="${heroes}">
            <form method="post" action="edithero">
            Hero name:${h.name} Description: ${h.description}
            <input type="hidden"  value="${h.name}" name="hName"/>
            <input type="hidden"  value="${h.description}" name="hDesc"/>
            <input type="submit" value="Edit" >
            </form>
            <form method="post" action="delhero">
            <input type="hidden"  value="${h.name}" name="hName"/>
            <input type="hidden"  value="${h.description}" name="hDesc"/>
            <input type="submit" value="${h.name} Delete hero" >
            <br>
            </form>
    </c:forEach>
    <br>
   
</html>

