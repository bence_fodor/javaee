<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form method="post" action="edithero">
        <h1>Edit hero:</h1><br>   
        Hero name: <input type="text" name="name" value="${edith.name}"> <br>
        Hero description: <input type="type" name="desc" value="${edith.description}" ><br>
        Hybrid:<br>
        <c:forEach var="spc" items="${species}">
            ${spc.name} <input type="text" name="${spc.name}">
        </c:forEach> <br>
        <input type="submit" value="OK" >
</form>
</html>
