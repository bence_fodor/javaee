<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form method="post" action="addnewhero">
        <h1>Add new hero</h1><br>   
        Hero name: <input type="text" name="name" > <br>
        Hero description : <input type="type" name="desc" ><br>
        Hybrid:<br>
        <c:forEach var="spc" items="${species}">
           ${spc.name} (${spc.description}) <input type="text" name="${spc.name}">
        </c:forEach> <br>
        <input type="submit" value="OK">
</form>
</html>