package hu.oe.hoe.web;

import hu.oe.hoe.adatok.Hero;
import hu.oe.hoe.adatok.HeroRepository;
import hu.oe.hoe.adatok.Hybrid;
import hu.oe.hoe.adatok.HybridRepository;
import hu.oe.hoe.adatok.NaturalAssetRepository;
import hu.oe.hoe.adatok.Species;
import hu.oe.hoe.adatok.SpeciesRepository;
import hu.oe.hoe.adatok.User;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "HeroServlet", urlPatterns = {"/addnewhero"})
public class HeroServlet extends HttpServlet {
    
    @Inject
    SpeciesRepository speciesRepository;
    
    @Inject
    HeroRepository heroRepository;
    
    @Inject
    HybridRepository hybridRepository;
    
     @Inject
    NaturalAssetRepository naturalAssetRepository;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Hero hero = new Hero(request.getParameter("name"), request.getParameter("desc"));
        for (Species sp : SpeciesRepository.instance.getSpecies()) {
            Hybrid nh = new Hybrid(sp, Byte.parseByte(request.getParameter(sp.getName())));
            HybridRepository.instance.getHybrids().add(nh);
            hero.getHybrid().add(nh);
        }
       HeroRepository.instance.add(hero);
       
       User u = ((User) request.getSession().getAttribute("user"));
       u.getHeroes().add(hero);
       
       request.getSession().setAttribute("user", u);
       request.setAttribute("heroes", u.getHeroes());
       request.setAttribute("empires", u.getEmpires());
       request.setAttribute("uName", u.getName());
        request.setAttribute("assets", naturalAssetRepository.getAssets());
       getServletContext().getRequestDispatcher("/main.jsp").include(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
