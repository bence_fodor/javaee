package hu.oe.hoe.web;

import hu.oe.hoe.adatok.NaturalAsset;
import hu.oe.hoe.adatok.NaturalAssetRepository;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "SearchNaturalAssetServlet", urlPatterns = {"/searchasset"})
public class SearchNaturalAssetServlet extends HttpServlet {

    
    @Inject
    NaturalAssetRepository naturalAssetRepository;
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      long id=Long.parseLong(request.getParameter("ID"));
        for(NaturalAsset b: naturalAssetRepository.searchAssets(id)){
            response.getWriter().print(b.getName()+": "+b.getDescription()+"\n");
        }    
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
