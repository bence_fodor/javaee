package hu.oe.hoe.web;

import hu.oe.hoe.adatok.LoginException;
import hu.oe.hoe.adatok.NaturalAssetRepository;
import hu.oe.hoe.adatok.User;
import hu.oe.hoe.adatok.UserRepository;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    @Inject
    UserRepository userRepository;
    
    @Inject
    NaturalAssetRepository naturalAssetRepository;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String name = request.getParameter("name");
        String password = request.getParameter("password");

        try {
            User u=userRepository.getByNameAndPassword(name, password);
            request.getSession().setAttribute("user", u);   
            request.setAttribute("heroes",u.getHeroes());
            request.setAttribute("empires", u.getEmpires());
            request.setAttribute("uName", u.getName());
            request.setAttribute("assets", naturalAssetRepository.getAssets());
            getServletContext().getRequestDispatcher("/main.jsp").include(request, response);
        } catch (LoginException e) {
            response.getWriter().print("nem jó bejelenetkezés ");
        }
    }

  
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
