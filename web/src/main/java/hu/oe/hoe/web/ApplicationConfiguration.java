package hu.oe.hoe.web;

import hu.oe.hoe.adatok.Ability;
import hu.oe.hoe.adatok.AbilityRepository;
import hu.oe.hoe.adatok.Building;
import hu.oe.hoe.adatok.BuildingRepository;
import hu.oe.hoe.adatok.EmpireRepository;
import hu.oe.hoe.adatok.HeroRepository;
import hu.oe.hoe.adatok.Hybrid;
import hu.oe.hoe.adatok.HybridRepository;
import hu.oe.hoe.adatok.NaturalAsset;
import hu.oe.hoe.adatok.NaturalAssetRepository;
import hu.oe.hoe.adatok.People;
import hu.oe.hoe.adatok.PeopleRepository;
import hu.oe.hoe.adatok.Population;
import hu.oe.hoe.adatok.PopulationRepository;
import hu.oe.hoe.adatok.Quality;
import hu.oe.hoe.adatok.QualityRepository;
import hu.oe.hoe.adatok.Species;
import hu.oe.hoe.adatok.SpeciesRepository;
import hu.oe.hoe.adatok.Stock;
import hu.oe.hoe.adatok.StockRepository;
import hu.oe.hoe.adatok.User;
import hu.oe.hoe.adatok.UserRepository;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class ApplicationConfiguration {
    
    @Produces
    @ApplicationScoped
    public UserRepository createUserRepository(){
        UserRepository ur= new UserRepository();
        try{
            ur.add(new User("a","a",true));
            ur.add(new User("b","b",false));
        }
        catch(Exception e){e.printStackTrace();}
        return ur;
    }
    
    @Produces
    @ApplicationScoped
    public SpeciesRepository createSpeciesRepository(){
        SpeciesRepository sr= new SpeciesRepository();
        try{
            sr.add(new Species("ember","okos"));
            sr.add(new Species("torpe","kicsi"));
        }
        catch(Exception e){e.printStackTrace();}
        return sr;
    }
    
    @Produces
    @ApplicationScoped
    public HeroRepository createHeroRepository(){
        HeroRepository hr= new HeroRepository();
        return hr;
    }
    
    @Produces
    @ApplicationScoped
    public EmpireRepository createEmpireRepository(){
        EmpireRepository hr= new EmpireRepository();
        return hr;
    }
    
    @Produces
    @ApplicationScoped
    public NaturalAssetRepository createNaturalAssetRepository(){
        NaturalAssetRepository nar= new NaturalAssetRepository();
        try{
            nar.add(new NaturalAsset("ko","kemeny"));
            nar.add(new NaturalAsset("fa","puha"));
        }
        catch(Exception e){e.printStackTrace();}
        return nar;
    }
    
    @Produces
    @ApplicationScoped
    public PeopleRepository createPeopleRepository(){
        PeopleRepository pr= new PeopleRepository();
        try{
            pr.add(new People("oriasok","magasak"));
            pr.add(new People("torpek","kicsik"));
        }
        catch(Exception e){e.printStackTrace();}
        return pr;
    }
    
    @Produces
    @ApplicationScoped
    public HybridRepository createHybridRepository(){
        HybridRepository hr= new HybridRepository();
        try{
            hr.add(new Hybrid(new Species("torpe","kicsi"), Byte.parseByte("1")));
            hr.add(new Hybrid(new Species("orias","nagy"), Byte.parseByte("1")));
        }
        catch(Exception e){e.printStackTrace();}
        return hr;
    }
    
    @Produces
    @ApplicationScoped
    public AbilityRepository createAbilityRepository(){
        AbilityRepository ar= new AbilityRepository();
        try{
            ar.add(new Ability(Byte.parseByte("1"),Byte.parseByte("2"),Byte.parseByte("3")));
            ar.add(new Ability(Byte.parseByte("2"),Byte.parseByte("4"),Byte.parseByte("6")));
        }
        catch(Exception e){e.printStackTrace();}
        return ar;
    }
    
    @Produces
    @ApplicationScoped
    public BuildingRepository createBuildingRepository(){
        BuildingRepository br= new BuildingRepository();
        try{
            br.add(new Building("haz","kicsi"));
            br.add(new Building("var","nagy"));
        }
        catch(Exception e){e.printStackTrace();}
        return br;
    }
    
    @Produces
    @ApplicationScoped
    public PopulationRepository createPopulationRepository(){
       PopulationRepository pr= new PopulationRepository();
        try{
            pr.add(new Population(new People("torpek","kicsik"),10));
            pr.add(new Population(new People("oriasok","nagyok"),10));
        }
        catch(Exception e){e.printStackTrace();}
        return pr;
    }
    
    @Produces
    @ApplicationScoped
    public QualityRepository createQualityRepository(){
       QualityRepository qr= new QualityRepository();
        try{
            qr.add(new Quality("a","a"));
            qr.add(new Quality("b","b"));
        }
        catch(Exception e){e.printStackTrace();}
        return qr;
    }
    
    @Produces
    @ApplicationScoped
    public StockRepository createStockRepository(){
       StockRepository sr= new StockRepository();
        try{
            sr.add(new Stock(new NaturalAsset("ko","kemeny"),10));
            sr.add(new Stock(new NaturalAsset("fa","puha"),10));
        }
        catch(Exception e){e.printStackTrace();}
        return sr;
    }
    
    
}