package hu.oe.hoe.web;

import hu.oe.hoe.adatok.NaturalAsset;
import hu.oe.hoe.adatok.NaturalAssetRepository;
import hu.oe.hoe.adatok.User;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "NaturalAssetServlet", urlPatterns = {"/addnewasset"})
public class NaturalAssetServlet extends HttpServlet {

    @Inject
    NaturalAssetRepository naturalassetRepository;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        NaturalAsset asset = new NaturalAsset(request.getParameter("name"), request.getParameter("desc"));       
        naturalassetRepository.add(asset);        
        User sess = ((User) request.getSession().getAttribute("user"));
        request.getSession().setAttribute("user", sess);
        request.setAttribute("empires", sess.getEmpires());
        request.setAttribute("heroes", sess.getHeroes());
        request.setAttribute("uName", sess.getName());
         request.setAttribute("assets", naturalassetRepository.getAssets());
        getServletContext().getRequestDispatcher("/main.jsp").include(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
