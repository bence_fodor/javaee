package hu.oe.hoe.web;

import hu.oe.hoe.adatok.QualityRepository;
import hu.oe.hoe.adatok.SpeciesRepository;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MainHeroServlet", urlPatterns = {"/mainhero"})
public class MainHeroServlet extends HttpServlet {

    @Inject
    SpeciesRepository speciesRepository;

    @Inject
    QualityRepository qualityRepository;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                
        request.setAttribute("species", speciesRepository.getSpecies());
        request.setAttribute("qualities", qualityRepository.getQualities());
        getServletContext().getRequestDispatcher("/hero.jsp").include(request, response);

    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
