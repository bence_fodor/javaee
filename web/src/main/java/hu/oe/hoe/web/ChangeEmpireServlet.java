package hu.oe.hoe.web;

import hu.oe.hoe.adatok.Empire;
import hu.oe.hoe.adatok.EmpireRepository;
import hu.oe.hoe.adatok.NaturalAssetRepository;
import hu.oe.hoe.adatok.User;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ChangeEmpireServlet", urlPatterns = {"/editemp"})
public class ChangeEmpireServlet extends HttpServlet {
    
    @Inject
    EmpireRepository empireRepository;
    
     @Inject
    NaturalAssetRepository naturalAssetRepository;
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        User u=(User)request.getSession().getAttribute("user");
                    
        int i=0;
        int empireIdx=0;
        for(Empire e:u.getEmpires())
        {
            if(e.getName().equals(request.getSession().getAttribute("eNm")))
            {
                empireIdx=i;
            }
            i++;
        }
        
        String newName=request.getParameter("name");
        String newDesc=request.getParameter("desc");
        u.getEmpires().get(empireIdx).setName(newName);
        u.getEmpires().get(empireIdx).setDescription(newDesc);
       
        empireRepository.getEmpires();
       
       request.getSession().setAttribute("user",u );
       request.getSession().removeAttribute("eNm");
       request.setAttribute("heroes",u.getHeroes());
       request.setAttribute("empires", u.getEmpires());
       request.setAttribute("uName", u.getName());
        request.setAttribute("assets", naturalAssetRepository.getAssets());
       getServletContext().getRequestDispatcher("/main.jsp").include(request, response);
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
