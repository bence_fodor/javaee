package hu.oe.hoe.web;

import hu.oe.hoe.adatok.BuildingRepository;
import hu.oe.hoe.adatok.Empire;
import hu.oe.hoe.adatok.User;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MainAddBuildingToEmpireServlet", urlPatterns = {"/mainbuildingtoemp"})
public class MainAddBuildingToEmpireServlet extends HttpServlet {

    @Inject
    BuildingRepository buildingRepository;
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        User u = ((User) request.getSession().getAttribute("user"));
        
        int index=0;
        int eindex=0;
        for(Empire e: u.getEmpires())
        {
            if(e.getName().equals(request.getParameter("eName")))
            {
                eindex=index;
            }
            index++;
        }
        
        request.setAttribute("edite", u.getEmpires().get(eindex));
        request.setAttribute("buildings", buildingRepository.getBuildings());
        request.getSession().setAttribute("eNm",request.getParameter("eName"));
        getServletContext().getRequestDispatcher("/addbuildingtoempire.jsp").include(request, response);
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
