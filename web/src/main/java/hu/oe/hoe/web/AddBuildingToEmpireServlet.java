package hu.oe.hoe.web;

import hu.oe.hoe.adatok.Building;
import hu.oe.hoe.adatok.BuildingRepository;
import hu.oe.hoe.adatok.Empire;
import hu.oe.hoe.adatok.EmpireRepository;
import hu.oe.hoe.adatok.NaturalAssetRepository;
import hu.oe.hoe.adatok.User;
import java.io.IOException;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AddBuildingToEmpireServlet", urlPatterns = {"/addbuildingtoemp"})
public class AddBuildingToEmpireServlet extends HttpServlet {

    @Inject
    BuildingRepository buildingRepository;
    
    @Inject
    EmpireRepository  empireRepository;
    
    @Inject
    NaturalAssetRepository naturalAssetRepository;
    
    private EntityManager entityManager= Persistence.createEntityManagerFactory("HeroPU").createEntityManager();

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     
        User usr =(User)request.getSession().getAttribute("user");
        
        String buildingName=request.getParameter("building");
        
        int i=0;
        int buildingIdx=0;
        for(Building b: buildingRepository.getBuildings())
        {
            if(b.getName().equals(buildingName))
            {
                buildingIdx=i;
            }
            i++;
        }
        
        int j=0;
        int empireIdx=0;
        for(Empire e:usr.getEmpires())
        {
            if(e.getName().equals(request.getSession().getAttribute("eNm")))
            {
                empireIdx=j;
            }
            j++;
        }   
              
       usr.getEmpires().get(empireIdx).getBuildings().add(buildingRepository.getBuildings().get(buildingIdx));
       long eID=usr.getEmpires().get(empireIdx).getID();
       long bID=buildingRepository.getBuildings().get(buildingIdx).getID();
        
       entityManager.getTransaction().begin();
       entityManager.createNativeQuery("INSERT INTO empire_building (Empire_ID,buildings_ID) VALUES (?,?)")
               .setParameter(1,eID)
               .setParameter(2,bID)
               .executeUpdate();
       entityManager.getTransaction().commit();
       
       request.getSession().setAttribute("user",usr );
       request.getSession().removeAttribute("eNm");
       request.setAttribute("heroes",usr.getHeroes());
       request.setAttribute("empires", usr.getEmpires());
       request.setAttribute("uName", usr.getName());
        request.setAttribute("assets", naturalAssetRepository.getAssets());
       getServletContext().getRequestDispatcher("/main.jsp").include(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}