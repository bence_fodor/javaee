package hu.oe.hoe.web;

import hu.oe.hoe.adatok.Empire;
import hu.oe.hoe.adatok.EmpireRepository;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "SearchEmpireServlet", urlPatterns = {"/search"})
public class SearchEmpireServlet extends HttpServlet {

   @Inject
   EmpireRepository empireRepository;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       String name=request.getParameter("Name");
       String desc=request.getParameter("Desc");
       String buildingID=request.getParameter("Building");
       long bID=Long.parseLong(buildingID);
       String minAsset=request.getParameter("Asset");
       long quantity=Long.parseLong(minAsset);
       String selectedAssetID=request.getParameter("SelectedAsset");
       long aID=Long.parseLong(selectedAssetID);
       String[] checkedIn=request.getParameterValues("condition");
       
      boolean nev=false;
      boolean leiras=false;
      boolean epulet=false;
      boolean asvany=false;
      
      for(int i=0;i<checkedIn.length;i++){
          if(checkedIn[i].equals("IsName")){nev=true;}
          if(checkedIn[i].equals("IsDesc")){leiras=true;}
          if(checkedIn[i].equals("IsBuilding")){epulet=true;}
          if(checkedIn[i].equals("IsAsset")){asvany=true;}
      }
      
     empireRepository.searchEmpire(nev, leiras, epulet, asvany, name, desc, bID, quantity, aID);
      
      for(Empire e: empireRepository.searchEmpire(nev, leiras, epulet, asvany,
              name, desc, bID, quantity, aID)){
            response.getWriter().print(e.getName()+": "+e.getDescription()+"\n");
        }
      
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
