package hu.oe.hoe.web;

import hu.oe.hoe.adatok.Empire;
import hu.oe.hoe.adatok.Hero;
import hu.oe.hoe.adatok.Hybrid;
import hu.oe.hoe.adatok.Species;
import hu.oe.hoe.adatok.SpeciesRepository;
import hu.oe.hoe.adatok.User;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "EmpireServlet", urlPatterns = {"/newempire"})
public class EmpireServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Empire empire = new Empire(request.getParameter("name"), request.getParameter("description"),
                Long.parseLong(request.getParameter("level")));

        User sess = ((User) request.getSession().getAttribute("user"));
        sess.getEmpires().add(empire);
        getServletContext().getRequestDispatcher("/user.jsp").include(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
