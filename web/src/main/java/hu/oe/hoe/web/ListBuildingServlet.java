package hu.oe.hoe.web;

import hu.oe.hoe.adatok.Building;
import hu.oe.hoe.adatok.BuildingRepository;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ListBuildingServlet", urlPatterns = {"/listbuilding"})
public class ListBuildingServlet extends HttpServlet {

    @Inject
    BuildingRepository buildingRepository;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        for(Building b: buildingRepository.listBuildings()){
            response.getWriter().print(b.getName()+": "+b.getDescription()+"\n");
        } 
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
