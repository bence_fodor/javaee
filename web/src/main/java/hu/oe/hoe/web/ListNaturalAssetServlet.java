package hu.oe.hoe.web;

import hu.oe.hoe.adatok.NaturalAsset;
import hu.oe.hoe.adatok.NaturalAssetRepository;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ListNaturalAssetServlet", urlPatterns = {"/listassets"})
public class ListNaturalAssetServlet extends HttpServlet {

    @Inject
    NaturalAssetRepository naturalAssetRepository;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        for(NaturalAsset a: naturalAssetRepository.listAssets()){
            response.getWriter().print(a.getName()+": "+a.getDescription()+"\n");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
