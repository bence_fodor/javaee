package hu.oe.hoe.web;

import hu.oe.hoe.adatok.HeroRepository;
import hu.oe.hoe.adatok.Hybrid;
import hu.oe.hoe.adatok.HybridRepository;
import hu.oe.hoe.adatok.NaturalAssetRepository;
import hu.oe.hoe.adatok.Species;
import hu.oe.hoe.adatok.SpeciesRepository;
import hu.oe.hoe.adatok.User;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ChangeHeroServlet", urlPatterns = {"/edithero"})
public class ChangeHeroServlet extends HttpServlet {

    @Inject
    HybridRepository hybridRepository;
    
    @Inject
    HeroRepository heroRepository;
    
    @Inject
    NaturalAssetRepository naturalAssetRepository;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        User usr=(User)request.getSession().getAttribute("user");
        int heroIdx = HeroRepository.instance.findHero(request.getSession().getAttribute("hNm").toString(),request.getSession().getAttribute("hD").toString());
      
        String newName=request.getParameter("name");
        String newDesc=request.getParameter("desc");
        usr.getHeroes().get(heroIdx).setName(newName);
        usr.getHeroes().get(heroIdx).setDescription(newDesc);

        for(Hybrid hyb :usr.getHeroes().get(heroIdx).getHybrid())
        {
            HybridRepository.instance.deleteHybrid(HybridRepository.instance.findHybrid(hyb.getSpecies(), hyb.getPercent()));
        }
        usr.getHeroes().get(heroIdx).getHybrid().clear();
       

        for (Species sp : SpeciesRepository.instance.getSpecies()) {
            Hybrid nh = new Hybrid(sp, Byte.parseByte(request.getParameter(sp.getName())));
            HybridRepository.instance.getHybrids().add(nh);
            usr.getHeroes().get(heroIdx).getHybrid().add(nh);
        }

       HeroRepository.instance.getHeroes(); //modify
       
       request.getSession().setAttribute("user",usr );
       request.getSession().removeAttribute("hNm");
       request.getSession().removeAttribute("hD");
       request.setAttribute("heroes",usr.getHeroes());
       request.setAttribute("empires", usr.getEmpires());
       request.setAttribute("uName", usr.getName());
       request.setAttribute("assets", naturalAssetRepository.getAssets());
       getServletContext().getRequestDispatcher("/main.jsp").include(request, response);
      
        
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}