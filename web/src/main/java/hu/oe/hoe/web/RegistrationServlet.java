package hu.oe.hoe.web;

import hu.oe.hoe.adatok.LoginException;
import hu.oe.hoe.adatok.RegistrationException;
import hu.oe.hoe.adatok.UserRepository;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "RegistrationServlet", urlPatterns = {"/reg"})
public class RegistrationServlet extends HttpServlet {

    @Inject
    UserRepository userRepository;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String password = request.getParameter("password");

        try {         
            userRepository.getByNameAndPassword(name, password);
        } catch (LoginException e) {
            response.getWriter().print("nem sikerült");
        }
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
