package hu.oe.hoe.web;

import hu.oe.hoe.adatok.Building;
import hu.oe.hoe.adatok.BuildingRepository;
import hu.oe.hoe.adatok.Empire;
import hu.oe.hoe.adatok.NaturalAssetRepository;
import hu.oe.hoe.adatok.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "BuildingServlet", urlPatterns = {"/addnewbuilding"})
public class BuildingServlet extends HttpServlet {
    
    @Inject
    BuildingRepository buildingRepository;

    @Inject
    NaturalAssetRepository naturalAssetRepository;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Building building = new Building(request.getParameter("name"), request.getParameter("desc"));       
        buildingRepository.add(building);        
        User sess = ((User) request.getSession().getAttribute("user"));
        request.getSession().setAttribute("user", sess);
        request.setAttribute("empires", sess.getEmpires());
        request.setAttribute("heroes", sess.getHeroes());
        request.setAttribute("uName", sess.getName());
        request.setAttribute("assets", naturalAssetRepository.getAssets());
        getServletContext().getRequestDispatcher("/main.jsp").include(request, response);
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
