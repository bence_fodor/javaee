package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="empire")
public class Empire {

    private String name, description;
    private long level;
    private List<Population> population = new ArrayList<>();    
    
    @OneToMany(cascade=CascadeType.ALL)
    @JoinTable(name="empire_stock",
            joinColumns=@JoinColumn(name="empireID"), 
            inverseJoinColumns=@JoinColumn(name="buildingID")
    )
    private List<Stock> produce = new ArrayList<>();
    
    @OneToMany(cascade=CascadeType.ALL)
    @JoinTable(name="empire_building",
            joinColumns=@JoinColumn(name="empireID"), 
            inverseJoinColumns=@JoinColumn(name="buildingID")
    )
    private List<Building> buildings = new ArrayList<>();
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long ID;

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }
    
    public List<Building> getBuildings() {
        return buildings;
    }

    public void setBuildings(List<Building> buildings) {
        this.buildings = buildings;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getLevel() {
        return level;
    }

    public void setLevel(long level) {
        this.level = level;
    }

    public List<Population> getPopulation() {
        return population;
    }

    public void setPopulation(List<Population> population) {
        this.population = population;
    }

    public List<Stock> getProduce() {
        return produce;
    }

    public void setProduce(List<Stock> produce) {
        this.produce = produce;
    }

    public Empire(String name, String description, long level) {
        this.name = name;
        this.description = description;
        this.level = level;
    }
    
    public Empire(String name, String description) {
        this.name = name;
        this.description = description;
    }
    
     public Empire() {
     
    }
   
}
