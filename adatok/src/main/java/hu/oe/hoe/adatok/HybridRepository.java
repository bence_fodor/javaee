/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;

public class HybridRepository {
    
    public static final HybridRepository instance = new HybridRepository();
    private List<Hybrid> hybrids = new ArrayList<>();

    public HybridRepository() {
       hybrids.add(new Hybrid(new Species("torpe","kicsi"), Byte.parseByte("1")));
       hybrids.add(new Hybrid(new Species("orias","nagy"), Byte.parseByte("1")));
    }

    public List<Hybrid> getHybrids() {
        return hybrids;
    }

    public void setHybrids(List<Hybrid> hybrids) {
        this.hybrids = hybrids;
    }
    
    public void add(Hybrid pValue){this.hybrids.add(pValue);}
    
    public int findHybrid(Species sp, byte percent)
    {
       return findHybridInList(this.hybrids, sp, percent);
    }
    
    public int findHybrid(Hybrid hybrid)
    {
       return findHybridInList(this.hybrids, hybrid);
    }
    
    public void deleteHybrid(int index)
    {
        this.hybrids.remove(index);
    }
    
    public void modifyHybrid(int hIndex,Species sp,byte percent )
    {
        this.hybrids.get(hIndex).modifyHybrid(sp, percent);
    }
    
    public void modifyHybrid(int hIndex,Hybrid hybrid )
    {
        this.hybrids.get(hIndex).modifyHybrid(hybrid);
    }
    
    public int findHybridInList(List<Hybrid> lh, Species sp, byte percent)
    {
        int index=-1;
        int i=0;
        for(Hybrid h: lh)
        {
            if(h.getSpecies().getName().equals(sp.getName()) && h.getSpecies().getDescription().equals(sp.getDescription()) && h.getPercent()==percent)
            {
                index=i;
            }
            i++;
        }
        return index;
    }
    
    public int findHybridInList(List<Hybrid> lh, Hybrid hybrid)
    {
        int index=-1;
        int i=0;
        for(Hybrid h: lh)
        {
            if(h.getSpecies().getName().equals(hybrid.getSpecies().getName()) && h.getSpecies().getDescription().equals(hybrid.getSpecies().getDescription()) && h.getPercent()==hybrid.getPercent())
            {
                index=i;
            }
            i++;
        }
        return index;
    }
    
    
}