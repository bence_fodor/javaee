package hu.oe.hoe.adatok;

public class Population {
    private People people;
    private long quantity;

    public Population() {
    }

    public Population(People people, long quantity) {
        this.people = people;
        this.quantity = quantity;
    }

    public People getPeople() {
        return people;
    }

    public void setPeople(People people) {
        this.people = people;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }
    
    
}
