/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 * @author javaee
 */
public class HeroRepository {

    @Inject
    HybridRepository hybridRepository; 
    
    @Inject
    QualityRepository qualityRepository;
    
   public static final HeroRepository instance = new HeroRepository();

   private List<Hero> heroes = new ArrayList<>();

   public HeroRepository() {
       heroes.add(new Hero("hos1", "okos"));
       heroes.add(new Hero("hos2", "kicsi"));
   }

    public List<Hero> getHeroes() {
        return heroes;
    }

    public void setHeroes(List<Hero> heroes) {
        this.heroes = heroes;
    }  
   
    public void add(Hero h){this.heroes.add(h);}
    
    public void add(String name, String desc){this.heroes.add(new Hero(name,desc));}
  
    public int findHero(String name, String desc)
    {
       return findHeroInList(this.heroes, name, desc);
    }
    
    public int findHero(Hero hero)
    {
       return findHeroInList(this.heroes, hero);
    }
    
    public void deleteHero(int index)
    {
        for(Hybrid hyb:heroes.get(index).getHybrid())
        {
            HybridRepository.instance.deleteHybrid(HybridRepository.instance.findHybrid(hyb.getSpecies(), hyb.getPercent()));
        }
        this.heroes.remove(index);
    }
    
    public void modifyHero(int hIndex, String newName, String newDesc)
    {
        this.heroes.get(hIndex).modifyHero(newName, newDesc);
    }
    
    public void modifyHero(int hIndex, Hero newHero)
    {
        this.heroes.get(hIndex).modifyHero(newHero);
    }
    
    public void modifyHeroWithQualities(int hIndex, Hero newHero)
    {
        this.heroes.get(hIndex).modifyHeroWithQualities(newHero);
    }
    
    public void modifyHeroWithHybrids(int hIndex, Hero newHero)
    {
        this.heroes.get(hIndex).modifyHeroWithHybrids(newHero);
    }
    
    public void modifyHeroWithHybridsAndQualities(int hIndex, Hero newHero)
    {
        this.heroes.get(hIndex).modifyHeroWithHybridsAndQualities(newHero);
    }
    
    public void addQualityToHero(int heroIndex, String qName, String qDesc)
    {
       Quality q= new Quality(qName, qDesc);
        QualityRepository.instance.add(q);
        this.heroes.get(heroIndex).getQualities().add(q);
    }
    
    public void addQualityToHero(int heroIndex, Quality newQuality)
    {
        QualityRepository.instance.add(newQuality);
        this.heroes.get(heroIndex).getQualities().add(newQuality);
    }
    
    public void deleteQualityFromHero(int heroIndex,String qName, String qDesc)
    {
//        int qIndex=QualityRepository.instance.findQualityInList(this.heroes.get(heroIndex).getQualities(), qName, qDesc);
//        this.heroes.get(heroIndex).getQualities().remove(qIndex);
//        QualityRepository.instance.deleteQuality(QualityRepository.instance.findQuality(qName, qDesc));
    }
    
    public void deleteQualityFromHero(int heroIndex,Quality q)
    {
//        int qIndex=QualityRepository.instance.findQualityInList(this.heroes.get(heroIndex).getQualities(), q);
//        this.heroes.get(heroIndex).getQualities().remove(qIndex);
//        QualityRepository.instance.deleteQuality(QualityRepository.instance.findQuality(q));
    }
    
    public void addHybridToHero(int heroIndex, Species sp, byte percent)
    {
       Hybrid h= new Hybrid(sp, percent);
        HybridRepository.instance.add(h);
        this.heroes.get(heroIndex).getHybrid().add(h);
    }
    
    public void addHybridToHero(int heroIndex, Hybrid hyb)
    {
        HybridRepository.instance.add(hyb);
        this.heroes.get(heroIndex).getHybrid().add(hyb);
    }
    
    public void deleteHybridFromHero(int heroIndex, Species sp, byte percent)
    {
        int hybIndex=HybridRepository.instance.findHybridInList(this.heroes.get(heroIndex).getHybrid(), sp, percent);
        this.heroes.get(heroIndex).getHybrid().remove(hybIndex);
        HybridRepository.instance.deleteHybrid(HybridRepository.instance.findHybrid(sp, percent));
    }
    
    public void deleteHybridFromHero(int heroIndex, Hybrid oldHybrid)
    {
        int hybIndex=HybridRepository.instance.findHybridInList(this.heroes.get(heroIndex).getHybrid(), oldHybrid);
        this.heroes.get(heroIndex).getHybrid().remove(hybIndex);
        HybridRepository.instance.deleteHybrid(HybridRepository.instance.findHybrid(oldHybrid));
    }
    
    public void modifyQualityInHero(int heroIndex, String qName, String qDesc, String newName, String newDesc)
    {
        int qIndex=QualityRepository.instance.findQualityInList(this.heroes.get(heroIndex).getQualities(), qName, qDesc);
        QualityRepository.instance.modifyQuality(heroIndex, newName, newDesc);
        this.heroes.get(heroIndex).getQualities().get(qIndex).modifyQuality(newName, newDesc);
    }
    
    public void modifyQualityInHero(int heroIndex, Quality oldQuality, String newName, String newDesc)
    {
        int qIndex=QualityRepository.instance.findQualityInList(this.heroes.get(heroIndex).getQualities(),oldQuality);
        QualityRepository.instance.modifyQuality(heroIndex, newName, newDesc);
        this.heroes.get(heroIndex).getQualities().get(qIndex).modifyQuality(newName, newDesc);
    }
    
    public void modifyQualityInHero(int heroIndex, String qName, String qDesc, Quality newQuality)
    {
        int qIndex=QualityRepository.instance.findQualityInList(this.heroes.get(heroIndex).getQualities(),qName, qDesc);
        QualityRepository.instance.modifyQuality(heroIndex, newQuality);
        this.heroes.get(heroIndex).getQualities().get(qIndex).modifyQuality(newQuality);
    }
    
    public void modifyQualityInHero(int heroIndex, Quality oldQuality, Quality newQuality)
    {
        int qIndex=QualityRepository.instance.findQualityInList(this.heroes.get(heroIndex).getQualities(),oldQuality);
        QualityRepository.instance.modifyQuality(heroIndex, newQuality);
        this.heroes.get(heroIndex).getQualities().get(qIndex).modifyQuality(newQuality);
    }
    
     public void modifyHybridInHero(int heroIndex, Species sp, byte percent, Species newSpecies, byte newPercent)
    {
        int hybIndex=HybridRepository.instance.findHybridInList(this.heroes.get(heroIndex).getHybrid(), sp, percent);
        HybridRepository.instance.modifyHybrid(heroIndex, newSpecies, newPercent);
        this.heroes.get(heroIndex).getHybrid().get(hybIndex).modifyHybrid(newSpecies, newPercent);
    }
    
    public void modifyHybridInHero(int heroIndex, Hybrid oldHybrid, Species newSpecies, byte newPercent)
    {
        int hybIndex=HybridRepository.instance.findHybridInList(this.heroes.get(heroIndex).getHybrid(),oldHybrid);
        HybridRepository.instance.modifyHybrid(heroIndex, newSpecies, newPercent);
        this.heroes.get(heroIndex).getHybrid().get(hybIndex).modifyHybrid(newSpecies, newPercent);
    }
    
    public void modifyHybridInHero(int heroIndex, Species sp, byte percent, Hybrid newHybrid)
    {
        int hybIndex=HybridRepository.instance.findHybridInList(this.heroes.get(heroIndex).getHybrid(),sp, percent);
       HybridRepository.instance.modifyHybrid(heroIndex, newHybrid);
        this.heroes.get(heroIndex).getHybrid().get(hybIndex).modifyHybrid(newHybrid);
    }
    
    public void modifyHybridInHero(int heroIndex, Hybrid oldHybrid, Hybrid newHybrid)
    {
        int hybIndex=HybridRepository.instance.findHybridInList(this.heroes.get(heroIndex).getHybrid(),oldHybrid);
        HybridRepository.instance.modifyHybrid(heroIndex, newHybrid);
        this.heroes.get(heroIndex).getHybrid().get(hybIndex).modifyHybrid(newHybrid);
    }
    
    public int findHeroInList(List<Hero> hl, String name, String desc)
    {
        int index=-1;
        int i=0;
        for(Hero h: hl)
        {
            if(h.getName().equals(name) && h.getDescription().equals(desc))
            {
                index=i;
            }
            i++;
        }
        return index;
    }
    
    public int findHeroInList(List<Hero> hl, Hero hero)
    {
        int index=-1;
        int i=0;
        for(Hero h: hl)
        {
            if(h.getName().equals(hero.getName()) && h.getDescription().equals(hero.getDescription()))
            {
                index=i;
            }
            i++;
        }
        return index;
    }
    
    
}

