package hu.oe.hoe.adatok;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="stock")
public class Stock {

    @ManyToOne
    private NaturalAsset naturalAsset;
    
    private long quantity;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long ID;
    
    public Stock() {
        
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }
    
    public NaturalAsset getNaturalAsset() {
        return naturalAsset;
    }

    public void setNaturalAsset(NaturalAsset naturalAsset) {
        this.naturalAsset = naturalAsset;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public Stock(NaturalAsset naturalAsset, long quantity) {
        this.naturalAsset = naturalAsset;
        this.quantity = quantity;
    }
}
