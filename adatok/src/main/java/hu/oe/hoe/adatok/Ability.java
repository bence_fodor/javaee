package hu.oe.hoe.adatok;

public class Ability {
    private byte force, brain, skill;

    public Ability() {
    }

    public Ability(byte force, byte brain, byte skill) {
        this.force = force;
        this.brain = brain;
        this.skill = skill;
    }

    public byte getForce() {
        return force;
    }

    public void setForce(byte force) {
        this.force = force;
    }

    public byte getBrain() {
        return brain;
    }

    public void setBrain(byte brain) {
        this.brain = brain;
    }

    public byte getSkill() {
        return skill;
    }

    public void setSkill(byte skill) {
        this.skill = skill;
    }
    
        public void modifyAbility(byte force, byte brain, byte skill)
    {
        this.force=force;
        this.brain=brain;
        this.skill=skill;
    }
    
    public void modifyAbility(Ability newAbility)
    {
        this.force=newAbility.force;
        this.brain=newAbility.brain;
        this.skill=newAbility.skill;
    }
    
}
