package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class StockRepository {
    
    private EntityManager entityManager = Persistence.createEntityManagerFactory("HeroPU").createEntityManager();

    public List<Stock> getStocks() {
        return entityManager.createQuery("SELECT s FROM Stock s ORDER BY name",Stock.class).getResultList();
    }

    public void add(Stock pValue){
        entityManager.getTransaction().begin();
        entityManager.persist(pValue);
        entityManager.getTransaction().commit();
    }
    
}
