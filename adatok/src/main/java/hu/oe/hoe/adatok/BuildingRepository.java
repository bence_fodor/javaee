package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class BuildingRepository {

    private EntityManager entityManager= Persistence.createEntityManagerFactory("HeroPU").createEntityManager();

    public List<Building> getBuildings() {
        return entityManager.createQuery(
                "SELECT b FROM Building b",
                Building.class).getResultList();
    }

    public void add(Building pValue){
        entityManager.getTransaction().begin();
        entityManager.persist(pValue);
        entityManager.getTransaction().commit();
    }

     public List<Building> listBuildings() {
      
        entityManager.getTransaction().begin();
        
        CriteriaBuilder critBuilder=entityManager.getCriteriaBuilder();        
        CriteriaQuery<Building> criteriaQuery=critBuilder.createQuery(Building.class);
        Root<Building> root=criteriaQuery.from(Building.class);
        criteriaQuery.orderBy(critBuilder.asc(root.get("name"))); //Novekvo
        TypedQuery<Building> tq= entityManager.createQuery(criteriaQuery.select(root));
        
        List<Building> result= tq.getResultList();
        entityManager.getTransaction().commit();
        
        return result;
    }   
     
     public List<Building> searchBuildings(long ID) {
      
        entityManager.getTransaction().begin();
        
        CriteriaBuilder critBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Building> criteriaQuery=critBuilder.createQuery(Building.class);
        Root<Building> root=criteriaQuery.from(Building.class);
        criteriaQuery.where(critBuilder.equal(root.get("ID"),ID));
        TypedQuery<Building> tq= entityManager.createQuery(criteriaQuery.select(root));
        
        List<Building> result= tq.getResultList();
        entityManager.getTransaction().commit();
        
        return result;
    }   
}

