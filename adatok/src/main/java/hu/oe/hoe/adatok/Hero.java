package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author javaee
 */
public class Hero {

    private String name, description;
    private List<Hybrid> hybrid = new ArrayList<>();
    private List<Quality> qualities = new ArrayList<>();

    public List<Quality> getQualities() {
        return qualities;
    }

    public void setQualities(List<Quality> qualities) {
        this.qualities = qualities;
    }

    public Hero(String name, String description) {
        this.name = name;
        this.description = description;

    }

    public Hero() {
    }

    public List<Hybrid> getHybrid() {
        return hybrid;
    }

    public void setHybrid(List<Hybrid> hybrid) {
        this.hybrid = hybrid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void modifyHero(String name, String desc)
    {
        this.name=name;
        this.description=desc;
    }
    
    public void modifyHero(Hero h)
    {
        this.name=h.getName();
        this.description=h.getDescription();
    }
    
    public void modifyHeroWithHybrids(Hero h)
    {
        this.name=h.getName();
        this.description=h.getDescription();
        this.hybrid=h.getHybrid();
    }
    
    public void modifyHeroWithQualities(Hero h)
    {
        this.name=h.getName();
        this.description=h.getDescription();
        this.qualities=h.getQualities();
    }
    
    public void modifyHeroWithHybridsAndQualities(Hero h)
    {
        this.name=h.getName();
        this.description=h.getDescription();
        this.hybrid=h.getHybrid();
        this.qualities=h.getQualities();
    }
    
}
