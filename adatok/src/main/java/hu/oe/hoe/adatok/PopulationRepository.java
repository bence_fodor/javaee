/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author javaee
 */
public class PopulationRepository {
 
    //singleton
    public static final PopulationRepository instance = new PopulationRepository();

    private List<Population> populations = new ArrayList<>();

    //singleton
    public PopulationRepository() {
        populations.add(new Population(new People("elfek","ugyesek"),10));
        populations.add(new Population(new People("felszerzetek","szoros a talpuk"),10));
    }

    public List<Population> getPopulations() {
        return populations;
    }

    public void setPopulations(List<Population> populations) {
        this.populations = populations;
    }
    
    
    
     public void add(Population pValue){this.populations.add(pValue);}
}
