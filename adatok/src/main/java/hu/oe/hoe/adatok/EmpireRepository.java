package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;


public class EmpireRepository 
{
    
    private EntityManager entityManager= Persistence.createEntityManagerFactory("HeroPU").createEntityManager();
    
    public List<Empire> getEmpires() 
    {
        return entityManager.createQuery("SELECT e FROM Empire e",Empire.class).getResultList();
    }
    
    public void add(Empire newEmpire)
    {
        entityManager.getTransaction().begin();
        
        for(Stock s: newEmpire.getProduce()){
            entityManager.persist(s);
        }
        
        entityManager.persist(newEmpire);
        entityManager.getTransaction().commit();
    }
   
    public List<Empire> searchEmpire(boolean isName, boolean isDesc, boolean isBuilding, boolean isAsset,
                                     String name, String desc, long buildingID, long quantity ,long selectedAssetID)
    {
        
        
        CriteriaBuilder critBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Empire> critQuery=critBuilder.createQuery(Empire.class);
        Root<Empire> root=critQuery.from(Empire.class);
        List<Predicate> predsList=new ArrayList<>();
        if(isName){predsList.add(critBuilder.like(root.get("name"),"%"+name+"%"));}
        if(isDesc){predsList.add(critBuilder.like(root.get("description"),"%"+desc+"%"));}
        if(isBuilding)
        {
            Join<Empire, Building> empBuilding=root.join("EmpireID",JoinType.INNER);
            predsList.add(critBuilder.equal(empBuilding.get("buildingIDs"),buildingID));
        }
        critQuery.where(critBuilder.and(predsList.toArray(new Predicate[predsList.size()])));
        
        TypedQuery<Empire> tq= entityManager.createQuery(critQuery.select(root));
        List<Empire> result= tq.getResultList();
                
        return result;
       
    }
}
