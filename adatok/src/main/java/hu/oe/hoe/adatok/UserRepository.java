package hu.oe.hoe.adatok;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class UserRepository {

    private EntityManager entityManager = Persistence.createEntityManagerFactory("HeroPU").createEntityManager();

    public UserRepository() {

    }

    public List<User> getUsers() {
        return entityManager.createQuery("SELECT s FROM User s", User.class).getResultList();
    }

    public User getByName(String userName) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(User.class);
        Root root = cq.from(User.class);
        cq.where(
                cb.equal(root.get("name"), userName)
        );

        List<User> result = entityManager.createQuery(cq).getResultList();
        if (result.isEmpty()) {
            return null;
        } else {
            return result.get(0);
        }

    }
    
    public User getByNameAndPassword(String userName, String password) throws LoginException {
        
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(User.class);
        Root root = cq.from(User.class);
        cq.where(
                cb.and(
                    cb.equal(root.get("name"), userName),
                    cb.equal(root.get("password"), password)
                )
        );

        List<User> result = entityManager.createQuery(cq).getResultList();
        if (result.isEmpty()) {
            throw new LoginException();
        } else {
            return result.get(0);
        }

    }

    public void add(User user) {
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
    }

    public void modify(User user) {
        entityManager.getTransaction().begin();
        entityManager.merge(user);
        entityManager.getTransaction().commit();
    }
}

