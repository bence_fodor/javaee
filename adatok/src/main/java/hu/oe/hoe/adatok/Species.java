package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;

public class Species {
    private String name, description;
    
    private List<Quality> qualities = new ArrayList<>();

    public Species(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public List<Quality> getQualities() {
        return qualities;
    }

    public void setQualities(List<Quality> qualities) {
        this.qualities = qualities;
    }

    public Species() {
    }    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
        public void modifySpecies(String name, String desc)
    {
        this.name=name;
        this.description=desc;
    }
    
    public void modifySpecies(Species newSpecies)
    {
        this.name=newSpecies.getName();
        this.description=newSpecies.getDescription();
    }
    
    public void modifySpeciesWithQualities(Species newSpecies)
    {
        this.name=newSpecies.getName();
        this.description=newSpecies.getDescription();
        this.qualities=newSpecies.getQualities();
    }

}
