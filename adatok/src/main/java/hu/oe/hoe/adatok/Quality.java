package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author javaee
 */
public class Quality {

    private String name, description;
    private List<Ability> ability = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Ability> getAbility() {
        return ability;
    }

    public void setAbility(List<Ability> ability) {
        this.ability = ability;
    }

    public Quality(String name, String description) {
        this.name = name;
        this.description = description;
    }
    
    public void modifyQuality(String name, String desc)
    {
        this.name=name;
        this.description=desc;
    }
    
    public void modifyQuality(Quality newQuality)
    {
        this.name=newQuality.getName();
        this.description=newQuality.getDescription();
    }
    
    public void modifyQualityWithAbilities(Quality newQuality)
    {
        this.name=newQuality.getName();
        this.description=newQuality.getDescription();
        this.ability=newQuality.getAbility();
    }
}
