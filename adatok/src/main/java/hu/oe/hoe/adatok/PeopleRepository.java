/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author javaee
 */
public class PeopleRepository {

    private EntityManager entityManager = Persistence.createEntityManagerFactory("HeroPU").createEntityManager();

    public PeopleRepository() {

    }

    public List<People> getPeople() {
        return entityManager.createQuery("SELECT s FROM People s", People.class).getResultList();
    }

    public void add(People p) {
        entityManager.getTransaction().begin();
        entityManager.persist(p);
        entityManager.getTransaction().commit();
    }

}
