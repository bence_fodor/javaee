package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="building")
public class Building {

    private String name, description;
    private long buildTime;
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long ID;

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getBuildTime() {
        return buildTime;
    }

    public void setBuildTime(long buildTime) {
        this.buildTime = buildTime;
    }

    public Building(String name, String description, long buildTime) {
        this.name = name;
        this.description = description;
        this.buildTime = buildTime;
    }
    
    public Building(String name, String description) {
        this.name = name;
        this.description = description;
    }
    
    public Building() {
       
    }
}
