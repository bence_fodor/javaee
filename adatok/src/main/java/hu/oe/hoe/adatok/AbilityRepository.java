package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;

public class AbilityRepository {
    
    public static final AbilityRepository instance = new AbilityRepository();

    private List<Ability> abilities = new ArrayList<>();

    public AbilityRepository() {
        abilities.add(new Ability(Byte.parseByte("5"),Byte.parseByte("1"),Byte.parseByte("3")));
        abilities.add(new Ability(Byte.parseByte("4"),Byte.parseByte("9"),Byte.parseByte("20")));
    }

    public List<Ability> getAbilities() {
        return abilities;
    }

    public void setAbilities(List<Ability> abilities) {
        this.abilities = abilities;
    }
    
    
    public void add(byte force, byte brain, byte skill) {
        this.abilities.add(new Ability(force, brain, skill));
    }
    
    public void add(Ability a){
        this.abilities.add(a);
    }
    
//    public int findAbility(byte force, byte brain, byte skill)
//    {
//       return findAbilityInList(this.abilities,force,brain,skill);
//    }
//    
//    public int findAbility(Ability a)
//    {
//       return findAbilityInList(this.abilities,a.getForce(),a.getBrain(),a.getSkill());
//    }
    
    public void deleteAbility(int index)
    {
        this.abilities.remove(index);
    }
    
    public void modifyAbility(int index, byte force, byte brain, byte skill)
    {
        this.abilities.get(index).modifyAbility(force, brain, skill);
    }
    
    public void modifyAbility(int index, Ability newAbility)
    {
        this.abilities.get(index).modifyAbility(newAbility);
    }
    
//    public int findAbilityInList(List<Ability> abilityList, byte force, byte brain, byte skill)
//    {
//        int idx=-1;
//        int i=0;
//        
//        for(Ability a: abilityList)
//        {
//            if(a.getForce()==force && a.getBrain()==brain && a.getSkill()==skill)
//            {
//                idx=i;
//            }
//        }
//        return idx;
//    }
//    
//    public int findAbilityInList(List<Ability> abilityList, Ability ability)
//    {
//        int idx=-1;
//        int i=0;
//        
//        for(Ability a: abilityList)
//        {
//            if(a.getForce()==ability.getForce() && a.getBrain()==ability.getBrain() && a.getSkill()==ability.getSkill())
//            {
//                idx=i;
//            }
//        }
//        return idx;
//    }
}
