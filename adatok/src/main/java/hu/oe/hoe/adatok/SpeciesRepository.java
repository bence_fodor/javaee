package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class SpeciesRepository {

    @Inject
    QualityRepository qualityRepository;
    
    public static final SpeciesRepository instance = new SpeciesRepository();

    private List<Species> species = new ArrayList<>();

    public SpeciesRepository() {
        species.add(new Species("ember", "okos"));
        species.add(new Species("torpe", "kicsi"));
    }

    public List<Species> getSpecies() {
        return species;
    }
    
    public void add(String name, String desc){this.species.add(new Species(name,desc));}
    
    public void add(Species s){this.species.add(s);}

    public int findSpecies(String name, String desc)
    {      
        return findSpeciesInList(this.species,name, desc);
    }
    
    public int findSpecies(Species species)
    {      
        return findSpeciesInList(this.species,species);
    }
    
    public void deleteSpecies(int index)
    {
//        for(Quality q: this.species.get(index).getQualities())
//        {
//            QualityRepository.instance.deleteQuality(QualityRepository.instance.findQuality(q.getName(),q.getDescription()));
//        }
        this.species.remove(index);
    }
    
    public void modifySpecies(int index, String name, String description)
    {
        this.species.get(index).modifySpecies(name, description);
    }
    
    public void modifySpecies(int index, Species newSpecies)
    {
        this.species.get(index).modifySpecies(newSpecies);
    }
     
    public void modifySpeciesWithQualities(int index, Species newSpecies)
    {
        this.species.get(index).modifySpeciesWithQualities(newSpecies);
    }  
    
    public void addQualityToSpecies(int spIndex, String qName, String qDesc)
    {
       Quality q= new Quality(qName, qDesc);
        QualityRepository.instance.add(q);
        this.species.get(spIndex).getQualities().add(q);
    }
    
    public void addQualityToSpecies(int spIndex, Quality newQuality)
    {
       
        QualityRepository.instance.add(newQuality);
        this.species.get(spIndex).getQualities().add(newQuality);
    }
    
    public void deleteQualityFromSpecies(int spIndex,String qName, String qDesc)
    {
//        int qIndex=QualityRepository.instance.findQualityInList(this.species.get(spIndex).getQualities(), qName, qDesc);
//        this.species.get(spIndex).getQualities().remove(qIndex);
//        QualityRepository.instance.deleteQuality(QualityRepository.instance.findQuality(qName, qDesc));
    }
    
    public void deleteQualityFromSpecies(int spIndex,Quality q)
    {
//        int qIndex=QualityRepository.instance.findQualityInList(this.species.get(spIndex).getQualities(), q);
//        this.species.get(spIndex).getQualities().remove(qIndex);
//        QualityRepository.instance.deleteQuality(QualityRepository.instance.findQuality(q));
    }
    
    public void modifyQualityInSpecies(int spIndex, String qName, String qDesc, String newName, String newDesc)
    {
        int qIndex=QualityRepository.instance.findQualityInList(this.species.get(spIndex).getQualities(), qName, qDesc);
        QualityRepository.instance.modifyQuality(spIndex, newName, newDesc);
        this.species.get(spIndex).getQualities().get(qIndex).modifyQuality(newName, newDesc);
    }
    
    public void modifyQualityInSpecies(int spIndex, Quality oldQuality, String newName, String newDesc)
    {
        int qIndex=QualityRepository.instance.findQualityInList(this.species.get(spIndex).getQualities(),oldQuality);
        QualityRepository.instance.modifyQuality(spIndex, newName, newDesc);
        this.species.get(spIndex).getQualities().get(qIndex).modifyQuality(newName, newDesc);
    }
    
    public void modifyQualityInSpecies(int spIndex, String qName, String qDesc, Quality newQuality)
    {
        int qIndex=QualityRepository.instance.findQualityInList(this.species.get(spIndex).getQualities(),qName, qDesc);
        QualityRepository.instance.modifyQuality(spIndex, newQuality);
        this.species.get(spIndex).getQualities().get(qIndex).modifyQuality(newQuality);
    }
    
    public void modifyQualityInSpecies(int spIndex, Quality oldQuality, Quality newQuality)
    {
        int qIndex=QualityRepository.instance.findQualityInList(this.species.get(spIndex).getQualities(),oldQuality);
        QualityRepository.instance.modifyQuality(spIndex, newQuality);
        this.species.get(spIndex).getQualities().get(qIndex).modifyQuality(newQuality);
    }
    
    public int findSpeciesInList(List<Species> sl, String name, String desc)
    {
        int index=-1;
        int i=0;
        for(Species s: sl)
        {
            if(s.getName().equals(name) && s.getDescription().equals(desc))
            {
                index=i;
            }
            i++;
        }
        return index;
    }
    
    public int findSpeciesInList(List<Species> sl, Species species)
    {
        int index=-1;
        int i=0;
        for(Species s: sl)
        {
            if(s.getName().equals(species.getName()) && s.getDescription().equals(species.getDescription()))
            {
                index=i;
            }
            i++;
        }
        return index;
    }
    
}
