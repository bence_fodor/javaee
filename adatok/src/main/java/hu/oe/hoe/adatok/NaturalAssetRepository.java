/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author javaee
 */
public class NaturalAssetRepository {
    
    private EntityManager em= Persistence.createEntityManagerFactory("HeroPU").createEntityManager();


    public List<NaturalAsset> getAssets() {
        return em.createQuery("SELECT a FROM NaturalAsset a",NaturalAsset.class).getResultList();
    }

    public void add(NaturalAsset pValue){
        em.getTransaction().begin();
        em.persist(pValue);
        em.getTransaction().commit();
    }
    
    public List<NaturalAsset> listAssets() {
      
        em.getTransaction().begin();
        
        CriteriaBuilder cb=em.getCriteriaBuilder();
        CriteriaQuery<NaturalAsset> cq=cb.createQuery(NaturalAsset.class);
        Root<NaturalAsset> root=cq.from(NaturalAsset.class);
        cq.orderBy(cb.asc(root.get("name")));
        TypedQuery<NaturalAsset> tq= em.createQuery(cq.select(root));
        
        List<NaturalAsset> result= tq.getResultList();
        em.getTransaction().commit();
        
        return result;
    }   
    
    public List<NaturalAsset> searchAssets(long ID) {
      
        em.getTransaction().begin();
        
        CriteriaBuilder cb=em.getCriteriaBuilder();
        CriteriaQuery<NaturalAsset> cq=cb.createQuery(NaturalAsset.class);
        Root<NaturalAsset> root=cq.from(NaturalAsset.class);
        cq.where(cb.equal(root.get("ID"),ID));
        TypedQuery<NaturalAsset> tq= em.createQuery(cq.select(root));
        
        List<NaturalAsset> result= tq.getResultList();
        em.getTransaction().commit();
        
        return result;
    }   
    
}
