package hu.oe.hoe.adatok;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class QualityRepository {
    
    @Inject
    AbilityRepository abilityRepository;
    
    public static final QualityRepository instance = new QualityRepository();

    private List<Quality> qualities = new ArrayList<>();

    public QualityRepository() {
        qualities.add(new Quality("a","a"));
        qualities.add(new Quality("b","b"));
    }

    public List<Quality> getQualities() {
        return qualities;
    }

    public void setQualities(List<Quality> qualities) {
        this.qualities = qualities;
    }

    public void add(String name, String desc){this.qualities.add(new Quality(name,desc));}
    
    public void add(Quality q){this. qualities.add(q);}
    
    public int findQuality(String name, String desc)
    {
        return findQualityInList(this.qualities,name, desc);
    }
    
    public int findQuality(Quality quality)
    {        
        return findQualityInList(this.qualities,quality);
    }
    
//    public void deleteQuality(int index)
//    {
//        for(Ability a: this.qualities.get(index).getAbility())
//        {
//            AbilityRepository.instance.deleteAbility(AbilityRepository.instance.findAbility(a.getForce(), a.getBrain(), a.getSkill()));
//        }
//        this.qualities.remove(index);
//    }
    
    public void modifyQuality(int index, String name, String desc)
    {
        this.qualities.get(index).modifyQuality(name, desc);
    }
    
    public void modifyQuality(int index, Quality newQuality)
    {
        this.qualities.get(index).modifyQuality(newQuality);
    }
    
    public void modifyQualityWithAbilities(int index, Quality newQuality)
    {
        this.qualities.get(index).modifyQualityWithAbilities(newQuality);
    }
    
    public void addAbilityToQuality(int index,byte force, byte brain, byte skill)
    {
        Ability a= new Ability(force, brain, skill);
        AbilityRepository.instance.add(a);
        this.qualities.get(index).getAbility().add(a);
    }
    
    public void addAbilityToQuality(int index, Ability newAbility)
    {       
        AbilityRepository.instance.add(newAbility);
        this.qualities.get(index).getAbility().add(newAbility);
    }
    
//    public void deleteAbilityFromQuality(int index,byte force, byte brain, byte skill)
//    {
//        int aIndex=AbilityRepository.instance.findAbilityInList(this.qualities.get(index).getAbility(), force, brain, skill);
//        this.qualities.get(index).getAbility().remove(aIndex);
//        AbilityRepository.instance.deleteAbility(AbilityRepository.instance.findAbility(force, brain, skill));
//    }
//    
//    public void deleteAbilityFromQuality(int index, Ability oldAbility)
//    {
//        int aIndex=AbilityRepository.instance.findAbilityInList(this.qualities.get(index).getAbility(), oldAbility);
//        this.qualities.get(index).getAbility().remove(aIndex);
//        AbilityRepository.instance.deleteAbility(AbilityRepository.instance.findAbility(oldAbility));
//    }
//    
//    public void modifyAbilityInQuality(int index, byte force, byte brain, byte skill, byte newForce, byte newBrain, byte newSkill)
//    {
//        int aIndex=AbilityRepository.instance.findAbilityInList(this.qualities.get(index).getAbility(), force, brain, skill);
//        AbilityRepository.instance.modifyAbility(index, newForce, newBrain, newSkill);
//        this.qualities.get(index).getAbility().get(aIndex).modifyAbility(newForce, newBrain, newSkill);
//    }
//    
//     public void modifyAbilityInQuality(int index, Ability oldAbility, byte newForce, byte newBrain, byte newSkill)
//    {
//        int aIndex=AbilityRepository.instance.findAbilityInList(this.qualities.get(index).getAbility(), oldAbility);
//        AbilityRepository.instance.modifyAbility(index, newForce, newBrain, newSkill);
//        this.qualities.get(index).getAbility().get(aIndex).modifyAbility(newForce, newBrain, newSkill);
//    }
//     
//      public void modifyAbilityInQuality(int index, byte force, byte brain, byte skill, Ability newAbility)
//    {
//        int aIndex=AbilityRepository.instance.findAbilityInList(this.qualities.get(index).getAbility(), force, brain, skill);
//        AbilityRepository.instance.modifyAbility(index, newAbility);
//        this.qualities.get(index).getAbility().get(aIndex).modifyAbility(newAbility);
//    }
//      
//      public void modifyAbilityInQuality(int index, Ability oldAbility, Ability newAbility)
//    {
//        int aIndex=AbilityRepository.instance.findAbilityInList(this.qualities.get(index).getAbility(), oldAbility);
//        AbilityRepository.instance.modifyAbility(index, newAbility);
//        this.qualities.get(index).getAbility().get(aIndex).modifyAbility(newAbility);
//    }
      
      public int findQualityInList(List<Quality> lq, String name, String desc)
      {
          int index=-1;
          int i=0;
          for(Quality q: lq)
          {
              if(q.getName().equals(name) && q.getDescription().equals(desc))
              {
                  index=i;
              }
              i++;
          }
          return index;
      }
      
       public int findQualityInList(List<Quality> lq, Quality quality)
      {
          int index=-1;
          int i=0;
          for(Quality q: lq)
          {
              if(q.getName().equals(quality.getName()) && q.getDescription().equals(quality.getDescription()))
              {
                  index=i;
              }
              i++;
          }
          return index;
      }
}
